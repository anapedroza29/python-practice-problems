# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(lst):
    
    

    if len(lst) % 2 == 0:
        half = len(lst) // 2
        new_list = lst[:half], lst[half:]
        return new_list
    else:
        half = (len(lst) // 2) + 1
        new_list = lst[:half], lst[half:]
        return new_list

   


print(halve_the_list([1, 2, 3, 4, 5]))
 