# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    
    lowercase = 0
    uppercase = 0
    digit = 0
    specialchar = 0
    character = 0

    for letter in password:
        if letter.islower():
            lowercase += 1
        if letter.isupper():
            uppercase += 1
        if letter.isdigit():
            digit += 1
        if letter.isalpha():
            character += 1
        if letter == "$" or letter == "!" or letter == "@":
            specialchar += 1

    if (len(password) >= 6 and len(password) <= 12) and lowercase >= 1 and uppercase >=1 and digit >=1 and specialchar >= 1 and character >= 1:
        return True
    else:
        return False
print(check_password("aS@9"))



